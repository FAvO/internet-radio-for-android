package de.favo.radio.playback;

import android.content.Context;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import de.favo.radio.tools.MetaDataRetriever;
import de.favo.radio.tools.radio.RadioStation;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class AudioPlayer implements Player.EventListener, AudioManager.OnAudioFocusChangeListener {

    private MetaDataRetriever mMetaDataRetriever;
    private SimpleExoPlayer mSimpleExoPlayer;
    private Callback mCallback;
    private Context mContext;
    private RadioStation mStation;
    private boolean buffering = false;

    AudioPlayer(Context c, Callback updates){
        mSimpleExoPlayer = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(c),
                new DefaultTrackSelector(),
                new DefaultLoadControl(
                        new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),
                        200,
                        5000,
                        200,
                        1000,
                        C.LENGTH_UNSET,
                        true
                    ));
        mSimpleExoPlayer.addListener(this);
        this.mCallback = updates;
        mContext = c;
    }

    public boolean isPlaying(){
        return mSimpleExoPlayer.getPlayWhenReady();
    }

    public int getAudioID(){
        return mSimpleExoPlayer.getAudioSessionId();
    }

    private Bundle getAudioInformation(Bundle existing){
        Format audio = mSimpleExoPlayer.getAudioFormat();
        if (existing == null)
            existing = new Bundle();

        if (mStation != null && audio != null){
            existing.putInt("channels",audio.channelCount);
            existing.putInt("bitrate",audio.bitrate);
            existing.putString("codec",audio.codecs);
            existing.putString("mimetype",audio.containerMimeType);
        }

        return existing;
    }

    public Bundle getExtraInformation(){
        Bundle b = new Bundle();
        b.putInt("audioID", mSimpleExoPlayer.getAudioSessionId());
        return b;
    }

    public void playStation(RadioStation station){
        MediaSource mediaSource = buildMediaSource(Uri.parse(station.getUrl()));
        this.mStation = station;
        if (mMetaDataRetriever != null && mMetaDataRetriever.isAlive()) {
            mMetaDataRetriever.setPause(true);
            mMetaDataRetriever.interrupt();
            mMetaDataRetriever = null;
        }
        mMetaDataRetriever = new MetaDataRetriever(new MetaDataRetriever.Callback() {
            @Override
            public void onStreamTitleChanged(String name) {
                mCallback.metadataChanged(name);
            }
            @Override
            public void onTextError() {
            }
            @Override
            public void onUrlError() {}
        },station.getUrl());

        mMetaDataRetriever.start();
        mSimpleExoPlayer.prepare(mediaSource, true, true);
        play();

    }

    public void close(){
        if (mMetaDataRetriever != null)
            mMetaDataRetriever.interrupt();
        stop(true);
        mSimpleExoPlayer.release();
    }

    public void stop(boolean reset){
        mSimpleExoPlayer.stop(reset);
        if (mMetaDataRetriever != null)
        mMetaDataRetriever.interrupt();
    }

    public void play(){
        if (mStation == null){
            mCallback.onNoSourceSelected();
            return;
        }
        if (requestAudioFocus()) {
            mSimpleExoPlayer.seekTo(0);
            mSimpleExoPlayer.setPlayWhenReady(true);
            mMetaDataRetriever.setPause(false);
        }
    }

    public void pause(){
        mSimpleExoPlayer.setPlayWhenReady(false);
        mMetaDataRetriever.setPause(true);
    }

    public void pauseTexts(){
        if (mMetaDataRetriever != null)
            mMetaDataRetriever.setPause(true);
    }
    public void resumeTexts(){
        if (mMetaDataRetriever != null)
            mMetaDataRetriever.setPause(false);
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        buffering = playbackState == Player.STATE_BUFFERING;
        mCallback.onBufferingStateChanged(buffering);
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {}

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) { }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        if (error.type == ExoPlaybackException.TYPE_SOURCE){
            if (error.getSourceException().getMessage().trim().startsWith("Unable to connect to")){
                mCallback.onSourceNotReachable();
            }
        }
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory(Util.getUserAgent(mContext,
                        "FAvO's Radio Stream Player"))).
                createMediaSource(uri);
    }

    private boolean requestAudioFocus() {
        AudioManager audioManager;
        android.media.AudioAttributes playbackAttributes;
        AudioFocusRequest focusRequest;

        audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            playbackAttributes = new android.media.AudioAttributes.Builder()
                    .setUsage(android.media.AudioAttributes.USAGE_MEDIA)
                    .setContentType(android.media.AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setAudioAttributes(playbackAttributes)
                    .setAcceptsDelayedFocusGain(true)
                    .setOnAudioFocusChangeListener(this)
                    .build();
            assert audioManager != null;
            return audioManager.requestAudioFocus(focusRequest)
                    == AudioManager.AUDIOFOCUS_GAIN;
        }else{
            assert audioManager != null;
            int result = audioManager.requestAudioFocus(this,
                    AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            return result == AudioManager.AUDIOFOCUS_GAIN;
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS: {
                if (mSimpleExoPlayer.getPlayWhenReady()) {
                    pause();
                }
                break;
            }
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT: {
                pause();
                break;
            }
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK: {
                if (mSimpleExoPlayer != null) {
                    mSimpleExoPlayer.setVolume(0.3f);
                }
                break;
            }
            case AudioManager.AUDIOFOCUS_GAIN: {
                if (mSimpleExoPlayer != null) {
                    if (!mSimpleExoPlayer.getPlayWhenReady()) {
                        play();
                    }
                    mSimpleExoPlayer.setVolume(1.0f);
                }
                break;
            }
        }

    }

    public RadioStation getStation() {
        return mStation;
    }

    public boolean isBuffering() {
        return buffering;
    }


    interface Callback{
        void metadataChanged(String text);
        void onBufferingStateChanged(boolean isBuffering);
        void onSourceNotReachable();
        void onNoSourceSelected();
    }
}
