package de.favo.radio.management;

import android.app.ActivityManager;
import android.graphics.BitmapFactory;
import android.support.v4.media.MediaMetadataCompat;

import de.favo.radio.R;
import de.favo.radio.tools.MarqueeToolbar;
import de.favo.radio.ui.RadioActivity;

import static android.media.MediaMetadata.METADATA_KEY_ARTIST;
import static android.support.v4.media.MediaMetadataCompat.METADATA_KEY_TITLE;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class PlayerTextsManager {

    private MarqueeToolbar mMarqueeToolbar;
    private boolean isBuffering = false;
    private String text, station;
    private RadioActivity mRadioActivity;

    public PlayerTextsManager(RadioActivity context){
        mMarqueeToolbar = context.findViewById(R.id.toolbar);
        mRadioActivity = context;
        reset();
    }

    private void update() {
        updateText();
        updateStation();
    }

    public void reset(){
        text = mRadioActivity.getString(R.string.app_name);
        station = mRadioActivity.getString(R.string.app_developer);
        mRadioActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                update();
            }
        });
    }

    public void updateUiSynchronously(final MediaMetadataCompat metadataCompat){
        mRadioActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                update(metadataCompat);
            }
        });
    }

    public boolean isBuffering() {
        return isBuffering;
    }

    private void update(MediaMetadataCompat metadataCompat){
        String  newText = metadataCompat.getString(METADATA_KEY_TITLE),
                newStation = metadataCompat.getString(METADATA_KEY_ARTIST);

        if (!newStation.equals(station)) {
            station = newStation;
            updateStation();
        }
        if (!newText.equals(text)) {
            text = newText;
            updateText();
        }
    }

    private void updateText() {
        mMarqueeToolbar.setTitle(text);
        ActivityManager.TaskDescription taskDescription =
                new ActivityManager.TaskDescription(text,
                        BitmapFactory.decodeResource(mRadioActivity.getResources(),
                                R.mipmap.ic_launcher_round), mRadioActivity.getRadioColor());
        mRadioActivity.setTaskDescription(taskDescription);
    }

    private void updateStation() {
        if (isBuffering)
            mMarqueeToolbar.setSubtitle(station + " — " +
                    mMarqueeToolbar.getContext().getString(R.string.text_state_buffering));
        else
            mMarqueeToolbar.setSubtitle(station);
        mMarqueeToolbar.invalidate();
    }

    public void setBuffering(boolean buffering) {
        isBuffering = buffering;
        updateStation();
    }
}
