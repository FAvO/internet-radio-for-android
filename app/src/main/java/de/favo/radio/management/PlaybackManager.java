package de.favo.radio.management;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import de.favo.radio.R;
import de.favo.radio.playback.RadioService;
import de.favo.radio.tools.BroadcastCallback;
import de.favo.radio.tools.Utils;
import de.favo.radio.ui.Splash;

/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
    public class PlaybackManager implements BroadcastCallback.Callback {
        private static final int NOTIFICATION_ID = 412;
        private static final int REQUEST_CODE = 100;

        private static final String ACTION_PAUSE = "de.favo.radio.pause";
        private static final String ACTION_PLAY = "de.favo.radio.play";
        private static final String ACTION_NEXT = "de.favo.radio.next";
        private static final String ACTION_PREV = "de.favo.radio.prev";

        private final RadioService mService;

        private final NotificationManager mNotificationManager;

        private final NotificationCompat.Action mPlayAction;
        private final NotificationCompat.Action mPauseAction;
        private final NotificationCompat.Action mNextAction;
        private final NotificationCompat.Action mPrevAction;
        private BroadcastCallback receiver;

        private boolean mStarted;

        public PlaybackManager(RadioService service) {
            mService = service;

            final String pkg = mService.getPackageName();
            PendingIntent playIntent =
                    PendingIntent.getBroadcast(
                            mService,
                            REQUEST_CODE,
                            new Intent(ACTION_PLAY).setPackage(pkg),
                            PendingIntent.FLAG_CANCEL_CURRENT);
            final PendingIntent pauseIntent =
                    PendingIntent.getBroadcast(
                            mService.getBaseContext(),
                            12634,
                            new Intent(ACTION_PAUSE).setPackage(pkg), 0);
            PendingIntent nextIntent =
                    PendingIntent.getBroadcast(
                            mService,
                            REQUEST_CODE+2,
                            new Intent(ACTION_NEXT).setPackage(pkg),
                            PendingIntent.FLAG_CANCEL_CURRENT);
            PendingIntent prevIntent =
                    PendingIntent.getBroadcast(
                            mService,
                            REQUEST_CODE+3,
                            new Intent(ACTION_PREV).setPackage(pkg),
                            PendingIntent.FLAG_CANCEL_CURRENT);

            mPlayAction =
                    new NotificationCompat.Action(
                            R.drawable.icon_play,
                            mService.getString(R.string.action_play),
                            playIntent);
            mPauseAction =
                    new NotificationCompat.Action(
                            R.drawable.icon_pause,
                            mService.getString(R.string.action_pause),
                            pauseIntent);
            mNextAction =
                    new NotificationCompat.Action(
                            R.drawable.icon_skip_to_next,
                            mService.getString(R.string.action_next),
                            nextIntent);
            mPrevAction =
                    new NotificationCompat.Action(
                            R.drawable.icon_previous,
                            mService.getString(R.string.action_previous),
                            prevIntent);

            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_NEXT);
            filter.addAction(ACTION_PAUSE);
            filter.addAction(ACTION_PLAY);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(ACTION_PREV);

            receiver = new BroadcastCallback(this);

            mService.registerReceiver(receiver, filter);

            mNotificationManager =
                    (NotificationManager) mService.getSystemService(Context.NOTIFICATION_SERVICE);

            // Cancel all notifications to handle the case where the Service was killed and
            // restarted by the system.
            assert mNotificationManager != null;
            mNotificationManager.cancelAll();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            assert action != null;
            switch (action) {
                case ACTION_PAUSE:
                    mService.mediaSessionCallback.onPause();
                    break;
                case ACTION_PLAY:
                    mService.mediaSessionCallback.onPlay();
                    break;
                case ACTION_NEXT:
                    mService.mediaSessionCallback.onSkipToNext();
                    break;
                case ACTION_PREV:
                    mService.mediaSessionCallback.onSkipToPrevious();
                    break;
            }
        }

        public void update(
                final MediaMetadataCompat metadata,
                PlaybackStateCompat state, boolean isBuffering,
                MediaSessionCompat.Token token) {
            if (state == null
                    || state.getState() == PlaybackStateCompat.STATE_STOPPED
                    || state.getState() == PlaybackStateCompat.STATE_NONE) {
                mService.stopForeground(true);
                try {
                    mService.unregisterReceiver(receiver);
                } catch (IllegalArgumentException ex) {
                    ex.printStackTrace();
                }
                mService.stopSelf();
                return;
            }
            if (metadata == null) {
                return;
            }

            boolean isPlaying =
                    state.getState() == PlaybackStateCompat.STATE_PLAYING ||
                            state.getState() == PlaybackStateCompat.STATE_BUFFERING;
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mService, Utils.PLAYER_CHANNEL_ID);

            notificationBuilder
                    .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle()
                                    .setMediaSession(token)
                                    .setShowActionsInCompactView(0, 1, 2))
                    .setColor(Color.parseColor(metadata.getString("color")))
                    .setSmallIcon(R.drawable.icon_radio)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setSubText(mService.getString(R.string.app_developer))
                    .setContentIntent(createContentIntent())
                    .setContentTitle(metadata.getString(MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE))
                    .setContentText(metadata.getString(MediaMetadataCompat.METADATA_KEY_ARTIST))
                    .setShowWhen(false)
                    .setUsesChronometer(false);

            // If skip to next action is enabled
            if ((state.getActions() & PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS) != 0) {
                notificationBuilder.addAction(mPrevAction);
            }
            notificationBuilder.addAction(isPlaying ? mPauseAction : mPlayAction);

            // If skip to prev action is enabled
            if ((state.getActions() & PlaybackStateCompat.ACTION_SKIP_TO_NEXT) != 0) {
                notificationBuilder.addAction(mNextAction);
            }

            if (isBuffering)
                notificationBuilder.setSubText(mService.getText(R.string.text_state_buffering));

            Notification notification = notificationBuilder.build();

            if (isPlaying && !mStarted) {

                Intent intent = new Intent(mService, RadioService.class);
                ContextCompat.startForegroundService(mService, intent);
                mService.startForeground(NOTIFICATION_ID, notification);
                mStarted = true;
            } else {
                if (!isPlaying) {
                    mService.stopForeground(false);
                    mStarted = false;
                }
                mNotificationManager.notify(NOTIFICATION_ID, notification);
            }
        }

        private PendingIntent createContentIntent() {
            Intent openUI = new Intent(mService, Splash.class);
            openUI.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            return PendingIntent.getActivity(
                    mService, 162356, openUI, PendingIntent.FLAG_CANCEL_CURRENT);
        }
}

