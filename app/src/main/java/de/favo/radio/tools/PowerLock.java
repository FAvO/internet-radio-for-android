package de.favo.radio.tools;

import android.content.Context;
import android.net.wifi.WifiManager;

import java.util.Objects;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class PowerLock {

    private WifiManager.WifiLock wifi;
    private android.os.PowerManager.WakeLock power;

    public PowerLock(Context c){
        try {

            wifi = ((WifiManager) Objects.requireNonNull(c.getApplicationContext()
                    // ggf. WIFI_MODE_FULL_HIGH_PERF
                    .getSystemService(Context.WIFI_SERVICE)))
                    .createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "de.favo.radio:wifi_lock");

            power = ((android.os.PowerManager) Objects.requireNonNull(c.getApplicationContext()
                    .getSystemService(Context.POWER_SERVICE)))
                    .newWakeLock(android.os.PowerManager.PARTIAL_WAKE_LOCK,"de.favo.radio:power_lock");

        }catch (Exception ignored){
        }
    }

    public void release(){
        wifi.release();
        power.release();
    }

    public void acquire(){
        wifi.acquire();
        power.acquire();
    }

}
