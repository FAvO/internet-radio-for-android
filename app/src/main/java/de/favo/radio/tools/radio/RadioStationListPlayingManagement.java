package de.favo.radio.tools.radio;

import android.content.Context;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class RadioStationListPlayingManagement {

    private RadioStationManager mRadioStationManagment;
    private RadioStation isPlaying;

    public RadioStationListPlayingManagement (Context c){
        mRadioStationManagment = new RadioStationManager(c);
        isPlaying = mRadioStationManagment.findByPosition(0);
    }

    public RadioStation getStationByNameForPlaying(String name){
        isPlaying = mRadioStationManagment.findRadioStationByName(name);
        return isPlaying;
    }

    public RadioStation getPlayingStation(){
        return isPlaying;
    }

    public RadioStation nextStation(){
        int pos = mRadioStationManagment.getPositionOf(isPlaying);

        if (pos < mRadioStationManagment.getRadioStations().size() - 1){
            isPlaying = mRadioStationManagment.findByPosition(pos + 1);
        }else{
            isPlaying = mRadioStationManagment.findByPosition(0);
        }

        return isPlaying;
    }

    public void reload(){
        mRadioStationManagment.reloadStations();
    }

    public RadioStation previousStation(){
        int pos = mRadioStationManagment.getPositionOf(isPlaying);

        if (pos > 0){
            isPlaying = mRadioStationManagment.findByPosition(pos - 1);
        }else{
            isPlaying = mRadioStationManagment
                    .findByPosition(mRadioStationManagment.getRadioStations().size() - 1);
        }

        return isPlaying;
    }
}
