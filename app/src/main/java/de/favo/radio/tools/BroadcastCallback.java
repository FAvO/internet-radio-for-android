package de.favo.radio.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class BroadcastCallback extends BroadcastReceiver {

    private Callback mCallback;

    public BroadcastCallback(Callback callback){
        this.mCallback = callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mCallback.onReceive(context, intent);
    }
    public interface Callback{
        void onReceive(Context context, Intent intent);
    }
}
