package de.favo.radio.ui;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.audiofx.AudioEffect;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import de.favo.radio.R;
import de.favo.radio.callbacks.MediaBrowserCallback;
import de.favo.radio.callbacks.MediaControllerCallback;
import de.favo.radio.management.PlayerTextsManager;
import de.favo.radio.management.ThemeManager;
import de.favo.radio.playback.RadioService;
import de.favo.radio.tools.Utils;
import de.favo.radio.tools.radio.RadioStation;
import de.favo.radio.tools.radio.RadioStationManager;
/*
    Radio - A simple internet radio
    Copyright (C) 2018  Felix v. Oertzen
    felix@von-oertzen-berlin.de

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class RadioActivity extends AppCompatActivity implements MediaBrowserCallback.ConnectionCallbackInternal, MediaControllerCallback.Callback {
    private static final int ERROR_UNKNOWN = 0;
    private static final int ERROR_NOT_CONNECTED_TO_MEDIA_BROWSER = 1;
    private static final int ERROR_PLAYBACK_ERROR = 2;
    private static final int ERROR_EQUALIZER_ERROR = 3;
    private static final int ERROR_NO_STATION_IS_PLAYING = 4;

    private RadioStationManager mRadioStationManager;
    private MediaBrowserCompat mMediaBrowser;
    private MenuItem MPlayPauseMenuItem;
    private RadioRecycler mRadioRecycler;
    private ThemeManager mThemeManager;
    private MediaControllerCallback mMediaControllerCallback;
    private PlayerTextsManager mPlayerTextsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mMediaBrowser = new MediaBrowserCompat(getApplicationContext(), new ComponentName(RadioActivity.this, RadioService.class),
                new MediaBrowserCallback(RadioActivity.this),
                null);
        mMediaControllerCallback = new MediaControllerCallback(RadioActivity.this);

        mRadioStationManager = new RadioStationManager(getApplicationContext());
        mRadioRecycler = findViewById(R.id.radio_view);
        mThemeManager = new ThemeManager(RadioActivity.this);
        mPlayerTextsManager = new PlayerTextsManager(RadioActivity.this);

        mRadioRecycler.setEmptyView(findViewById(R.id.radio_activity_empty));
        mRadioRecycler.updateList();

        Utils.setUpNotificationChannels(getApplicationContext());

        mPlayerTextsManager.reset();
    }

    public int getRadioColor(){
        if (mThemeManager == null)
            return Color.WHITE;
        else
            return mThemeManager.getColor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mMediaBrowser.isConnected()) {
            mMediaBrowser.connect();
            setEnabled(false);
        }
        mThemeManager.updateColor();
        mRadioRecycler.updateList();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
           mThemeManager.updateColor(savedInstanceState.getInt("color", Color.WHITE));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("color", mThemeManager.getColor());
    }

    @Override
    public void onStop() {
        super.onStop();
        if (MediaControllerCompat.getMediaController(RadioActivity.this) != null) {
            MediaControllerCompat.getMediaController(RadioActivity.this)
                    .unregisterCallback(mMediaControllerCallback);
        }
        mMediaBrowser.disconnect();
    }

    public void onPlayRadioStation(RadioStation station){
        getMediaController().getTransportControls().playFromSearch(station.getName(),null);
        mThemeManager.updateColor(station.getColor());
    }

    private void setEnabled(boolean isEnabled){
        if (!isEnabled)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void onRadioStationLongPressed(final RadioStation stationName){
        String[] actions = {getString(R.string.action_play), getString(R.string.action_edit),
                getString(R.string.action_delete), getString(R.string.action_edit_up), getString(R.string.action_edit_down)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(stationName.getName())
                .setItems(actions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item){
                            case 0:{
                                getMediaController().getTransportControls().playFromMediaId(stationName.getName(),null);
                                break;
                            }
                            case 1:{
                                Intent i = new Intent(getApplicationContext(),AddStationActivity.class);
                                i.putExtra("isEditing",true);
                                i.putExtra("name",stationName.getName());
                                startActivity(i);
                                break;
                            }
                            case 2:{
                                mRadioStationManager.deleteStationStation(mRadioStationManager.findRadioStationByName(stationName.getName()));
                                mRadioRecycler.updateList();
                                break;
                            }
                            case 3:{
                                mRadioStationManager.moveStationUp(stationName.getName());
                                mRadioRecycler.updateList();
                                break;
                            }
                            case 4:{
                                mRadioStationManager.moveStationDown(stationName.getName());
                                mRadioRecycler.updateList();
                                break;
                            }
                        }
                    }
                });
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_radio, menu);
        MPlayPauseMenuItem = menu.findItem(R.id.action_play_pause);
        if ((new Intent("android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL")).resolveActivity( getPackageManager()) != null) { menu.findItem(R.id.action_equalizer).setEnabled(true);menu.findItem(R.id.action_equalizer).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM); }
        return true;
    }

    private void togglePlayPause(){
        int pbState = MediaControllerCompat.getMediaController(RadioActivity.this).getPlaybackState().getState();
        if (pbState == PlaybackStateCompat.STATE_PLAYING) {
            MPlayPauseMenuItem.setIcon(mThemeManager.getDrawable(R.drawable.icon_pause));
            MediaControllerCompat.getMediaController(RadioActivity.this).getTransportControls().pause();
        } else {
            MPlayPauseMenuItem.setIcon(mThemeManager.getDrawable(R.drawable.icon_play));
            if (MediaControllerCompat.getMediaController(RadioActivity.this).getMetadata() == null)
                handleError(ERROR_NO_STATION_IS_PLAYING);
            else
                MediaControllerCompat.getMediaController(RadioActivity.this).getTransportControls().play();
        }
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_play_pause) {
            togglePlayPause();
        }
        if (id == R.id.action_equalizer){
            Intent intent = new Intent("android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL");
            if (intent.resolveActivity( getPackageManager()) != null) {
                if (getMediaController().getExtras() != null &&
                        getMediaController().getExtras()
                                .getInt("audioID",-1) != -1) {
                    intent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, getPackageName());
                    intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION,
                            getMediaController().getExtras().getInt("audioID",0));
                    startActivityForResult( intent , 100 );
                }else{
                    handleError(ERROR_UNKNOWN);
                }

            }else{
                handleError(ERROR_EQUALIZER_ERROR);
            }
        }
        /*if (id == R.id.action_info){
            if (getMediaController() != null && getMediaController().getPlaybackState() != null &&
                    getMediaController().getPlaybackState().getState() == PlaybackState.STATE_PLAYING) {
                StringBuilder sb = new StringBuilder(100);
                Bundle extras = getMediaController().getExtras();

                if (extras == null){
                    handleError(ERROR_UNKNOWN);
                }

                sb.append("<p>");
                if (!Objects.equals(extras.getString("mimetype"), null)) {
                    sb.append(getString(R.string.text_info_advanced_audio_info_mimetype));
                    sb.append("</p><tt>");
                    sb.append(extras.getString("mimetype"));
                    sb.append("</tt><br><p>");
                }
                if (extras.getInt("channels") != -1) {
                    sb.append(getString(R.string.text_info_advanced_audio_info_channels));
                    sb.append("</p><tt>");
                    sb.append(extras.getInt("channels"));
                    sb.append("</tt><br><p>");
                }

                if (extras.getInt("bitrate") != -1) {
                    sb.append(getString(R.string.text_info_advanced_audio_info_bitrate));
                    sb.append("</p><tt>");
                    sb.append(extras.getInt("bitrate"));
                    sb.append("</tt><br>");
                }

                if (Objects.equals(extras.getString("codec"), "null")) {
                    sb.append("<p>");
                    sb.append(getString(R.string.text_info_advanced_audio_info_codecs));
                    sb.append("</p><tt>");
                    sb.append(extras.getString("codec"));
                    sb.append("</tt><br><p>");
                }

                showHtmlDialog(getString(R.string.text_info_advanced_audio_info_header),sb.toString());
            }else {
                handleError(ERROR_NO_STATION_IS_PLAYING);
            }
        }*/
        if (id == R.id.action_about){
            startActivity(new Intent(getApplicationContext(), AboutActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public void handleError(int type){
        int[] texts = {R.string.text_error_occurred_unknown,
                R.string.text_error_occurred_cannot_connect_to_player,
                R.string.text_error_invalid_url,
                R.string.text_error_equalizer_not_found,
                R.string.text_error_no_station_is_playing};
        showDialog(R.string.text_error_occurred,texts[type]);
    }

    private void showDialog(int title, int text){
        showDialog(getString(title), getString(text));
    }
    private void showDialog(String title, String text){
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(R.string.action_got_it, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { }
                })
                .show();

    }
 
    public void addStation(View v){
        startActivity(new Intent(getApplicationContext(),AddStationActivity.class));
    }

    void buildTransportControls() {
        MediaControllerCompat mediaController = MediaControllerCompat.getMediaController(RadioActivity.this);
        MediaMetadataCompat metadata = mediaController.getMetadata();

        MPlayPauseMenuItem.setIcon(mThemeManager
                .getDrawable((mediaController.getPlaybackState().getState() == PlaybackStateCompat.STATE_PLAYING)
                        ? R.drawable.icon_pause : R.drawable.icon_play));

        if (metadata != null){
            mPlayerTextsManager.updateUiSynchronously(metadata);
            mThemeManager.updateColor(metadata.getString("color"));
        }else{
            mThemeManager.reset();
        }
        mediaController.registerCallback(this.mMediaControllerCallback);
    }


    @Override
    public void onConnected() {
        MediaSessionCompat.Token token = mMediaBrowser.getSessionToken();
        MediaControllerCompat mediaController = null;
        try {
            mediaController = new MediaControllerCompat(RadioActivity.this,token);
        } catch (RemoteException e) { handleError(ERROR_NOT_CONNECTED_TO_MEDIA_BROWSER); }
        MediaControllerCompat.setMediaController(RadioActivity.this, mediaController);
        buildTransportControls();
        setEnabled(true);
    }

    @Override
    public void onConnectionSuspended() {
        handleError(ERROR_UNKNOWN);
        setEnabled(true);
    }

    @Override
    public void onConnectionFailed() {
        handleError(ERROR_NOT_CONNECTED_TO_MEDIA_BROWSER);
        setEnabled(true);
    }

    @Override
    public void onExtrasChanged(Bundle bundle) {}

    @Override
    public void onPlaybackStateChanged(PlaybackStateCompat playbackState) {
        if (!(playbackState.getErrorCode() == PlaybackStateCompat.ERROR_CODE_END_OF_QUEUE ||
                playbackState.getErrorCode()  == PlaybackStateCompat.ERROR_CODE_NOT_AVAILABLE_IN_REGION)) {

            mPlayerTextsManager.setBuffering(playbackState.getState() == PlaybackStateCompat.STATE_BUFFERING);
            MPlayPauseMenuItem.setIcon(mThemeManager
                    .getDrawable((playbackState.getState() == PlaybackStateCompat.STATE_PLAYING)
                            ? R.drawable.icon_pause : R.drawable.icon_play));

        }else {
            mThemeManager.reset();
            mPlayerTextsManager.reset();
            if (playbackState.getErrorCode() == PlaybackStateCompat.ERROR_CODE_NOT_AVAILABLE_IN_REGION)
                handleError(ERROR_PLAYBACK_ERROR);
            if (playbackState.getErrorCode() == PlaybackStateCompat.ERROR_CODE_END_OF_QUEUE)
                showDialog(getString(R.string.text_error_occurred),
                        playbackState.getErrorMessage().toString());
        }

    }

    @Override
    public void onMetadataChanged(MediaMetadataCompat metadata) {
        mPlayerTextsManager.updateUiSynchronously(metadata);
    }
}
